﻿using PropremaZaTest_Duzi.Interfaces;
using PropremaZaTest_Duzi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace PropremaZaTest_Duzi.Repository
{
    public class ProizvodRepository : IDisposable, IProizvodRepository
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public void Create(Proizvod proizvod)
        {
            db.Proizvodi.Add(proizvod);
            db.SaveChanges();
        }

        public void Delete(Proizvod proizvod)
        {
            db.Proizvodi.Remove(proizvod);
            db.SaveChanges();
        }


        public IEnumerable<Proizvod> GetAll()
        {
            return db.Proizvodi.Include(p=>p.Kategorija);
        }

        public Proizvod GetById(int id)
        {
            return db.Proizvodi.FirstOrDefault(p => p.Id == id);
        }

        public void Update(Proizvod proizvod)
        {
            db.Entry(proizvod).State = EntityState.Modified;
            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }
        }
        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}