﻿using PropremaZaTest_Duzi.Interfaces;
using PropremaZaTest_Duzi.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace PropremaZaTest_Duzi.Repository
{
    public class KategorijaRepository : IDisposable, IKategorijaRepository
    {
        ApplicationDbContext db = new ApplicationDbContext();
        public void Create(Kategorija kategorija)
        {
            db.Kategorije.Add(kategorija);
            db.SaveChanges();
        }

        public void Delete(Kategorija kategorija)
        {
            db.Kategorije.Remove(kategorija);
            db.SaveChanges();
        }

        public IEnumerable<Kategorija> GetAll()
        {
            return db.Kategorije.Include(k=>k.Proizvodi);
        }

        public Kategorija GetById(int id)
        {
            return db.Kategorije.FirstOrDefault(k=>k.Id==id);
        }

        public void Update(Kategorija kategorija)
        {
            db.Entry(kategorija).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

        }

        protected void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (db != null)
                {
                    db.Dispose();
                    db = null;
                }
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}