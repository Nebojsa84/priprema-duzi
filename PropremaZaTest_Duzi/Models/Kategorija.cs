﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropremaZaTest_Duzi.Models
{
    public class Kategorija
    {
        public int Id { get; set; }
        [Required]
        public string Naziv { get; set; }

        public List<Proizvod> Proizvodi { get; set; }


    }
}