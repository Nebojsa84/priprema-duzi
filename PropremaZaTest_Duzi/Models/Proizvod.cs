﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PropremaZaTest_Duzi.Models
{
    public class Proizvod
    {
        public int Id { get; set; }
        [Required]
        public string Naziv { get; set; }

        [Range(0, 1000)]
        public decimal CenaProizvoda { get; set; }
        public int KategorijaId { get; set; }

        public Kategorija Kategorija { get; set; }

    }
}