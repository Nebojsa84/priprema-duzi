﻿$(document).ready(function () {

    var token = null;
    var headers = {};
    var host = window.location.host;

    $("#reg-div").css("display","none");
    $("#info-div").css("display", "none");
    $("#addNew-div").css("display", "none");

    $("body").on("click", "#delete-btn", obrisiProizvod);
    $("body").on("click", "#edit-btn", izmeniProizvod);

    getData();


    function obrisiProizvod() {

        var itemId = this.name;

        if (token) {
            headers.Authorization = "Bearer " + token;
        }

        $.ajax({

            type: "DELETE",
            url: "http://" + host + "/api/proizvodi/" + itemId,
            headers:headers
        }).done(function () {
            getData();

        }).fail(function () {
            alert("Doslo je do greske!");
       });

    }

    function izmeniProizvod() {

        var itemId = this.name;
 
        $.ajax({

            type: "GET",
            url: "http://" + host + "/api/proizvodi/" + itemId

        }).done(function (data) {

            $("#naziv-nov").val(data.Naziv);
            $("#cena-nov").val(data.CenaProizvoda);
            $("#kategorije-slc").val(data.KategorijaId).change();
        });


    }

    function getData() {

        $.ajax({

            type: "GET",
            url: "http://" + host + "/api/proizvodi"

        }).done(function (data) {

            showData(data);
        });

    }

    function showData(data) {

        var container = $("#table-div");
        container.empty();

        var table = $("<table/>").addClass("table table-bordered");
        var header = $("<thead><tr class='table - active'><th>Id</th><th>Naziv</th><th>Cena</th><th>Kategorija</th></tr></thead>");        
        var tableBody = $("<tbody/>");
        table.append(header);

        var i;
        for (i = 0; i < data.length; i++) {
            var row = $("<tr/>");

            row.append("<td>" + data[i].Id + "</td><td>" + data[i].Naziv + "</td><td>" + data[i].CenaProizvoda + "</td><td>" + data[i].Kategorija.Naziv + "</td>");
            if (token) {
                row.append("<td><button class='btn btn-danger' id='delete-btn' name=" + data[i].Id + " ><span class='glyphicon glyphicon-trash'></span></button></td>");
                row.append("<td><button class='btn btn-primary' id='edit-btn' name=" + data[i].Id + " ><span class='glyphicon glyphicon-pencil'></span></button></td>");
            }

            tableBody.append(row);
        }
        table.append(tableBody);
        container.append(table);
    }


    $("#reg-btn").click(function (e) {
        e.preventDefault();
        $("#reg-div").css("display", "block");
        $("#login-div").css("display", "none");

    });

    //registracija
    $("#registracija").submit(function (e) {
        e.preventDefault();
        var email = $("#regEmail").val();
        var password = $("#regLoz").val();
        var conPassword = $("#regLoz2").val();

        sendData = {

            "Email": email,
            "Password": password,
            "ConfirmPassword": conPassword

        };

        $.ajax({

            type: "POST",
            url: "http://" + host + "/api/Account/Register",
            data: sendData
        }).done(function () {
            alert("Uspesna registracija");
            $("#login-div").css("display", "block");
            $("#reg-div").css("display", "none");

        }).fail(function () {

            alert("Doslo je do greske!");

        });

    });


    //prijava korisnika
    $("#prijava").submit(function (e) {

        e.preventDefault();

        user = $("#priEmail").val();
        pass = $("#priLoz").val();

        sendData = {
            grant_type: "password",
            username: user,
            password: pass
        };

        $.ajax({

            type: "POST",
            url: "http://" + host + "/Token",
            data: sendData

        }).done(function (data) {

            alert("Uspesna prijava!");
            token = data.access_token;
            console.log(token);
            $("#login-div").css("display", "none");
            $("#info-div").css("display", "block");
            $("#addNew-div").css("display", "block");
            fillSelect();
            $("#info").empty().append("Prijavljen korisnik: " + data.userName);
            $("#priEmail").val("");
            $("#priLoz").val("");
            getData();
        }).fail(function (data) {

            console.log(data.responseText);
            alert(data.responseText);
        });

    });

    //odjava
    $("#btn-odjava").click(function () {

        $("#reg-div").css("display", "none");
        $("#login-div").css("display", "block");
        $("#info-div").css("display", "none");
        $("#addNew-div").css("display", "none");
        getData();
        token = null;
        headers = {};
    });


    function fillSelect() {

        $.ajax({

            type: "GET",
            url: "http://" + host + "/api/kategorije"
        })
            .done(function (data) {
                var select = $("#kategorije-slc");
                for (let i = 0; i < data.length; i++) {

                    select.append("<option value = " + data[i].Id + ">" + data[i].Naziv + "</option>");
                }
            });
    }


    $("#addNew").submit(function (e) {

        e.preventDefault();

        var naziv = $("#naziv-nov").val();
        var cena = $("#cena-nov").val();
        var kategorija = $("#kategorije-slc").val();

        sendData = {

            Naziv: naziv,
            CenaProizvoda: cena,
            KategorijaId:kategorija

        };

        console.log(sendData);

        if (token) {

            headers.Authorization = "Bearer " + token;
        }

        $.ajax({

            type: "POST",
            url: "http://" + host + "/api/proizvodi",
            headers: headers,
            data: sendData
        })
            .done(function (data) {
                getData();
                alert("Proizvod " + data.Naziv + " uspesno kreiran!");
                $("#naziv-nov").val("");
                $("#cena-nov").val("");
                $("#kategorije-slc").val("");
            })
            .fail(function () {

                alert("Doslo je do greske!");
            });

    }); 

    $("#odustani-btn").click(function (e) {
        e.preventDefault();
        $("#naziv-nov").val("");
        $("#cena-nov").val("");
        $("#kategorije-slc").val("");

    });

});