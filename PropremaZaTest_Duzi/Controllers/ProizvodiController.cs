﻿using PropremaZaTest_Duzi.Interfaces;
using PropremaZaTest_Duzi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PropremaZaTest_Duzi.Controllers
{
    public class ProizvodiController : ApiController
    {
        public IProizvodRepository _repository { get; set; }

        public ProizvodiController(IProizvodRepository repository)
        {
            _repository = repository;
        }

        public  IEnumerable<Proizvod> GetProizvodi()
        {
            return _repository.GetAll();
        }

        public IHttpActionResult GetProizvod(int id)
        {
            var proizvod = _repository.GetById(id);
            if (proizvod==null)
            {
                return NotFound();
            }

            return Ok(proizvod);
        }
        [Authorize]
        public IHttpActionResult DeleteProizvod(int id)
        {
            var proizvod = _repository.GetById(id);
            if (proizvod==null)
            {
                return NotFound();
            }
            _repository.Delete(proizvod);
            return Ok();
        }
        [Authorize]
        public IHttpActionResult PostProizvod(Proizvod proizvod)
        {
            _repository.Create(proizvod);
            return CreatedAtRoute("DefaultApi", new { proizvod.Id }, proizvod);
        }
        [Authorize]
        public IHttpActionResult PutProizvod(int id,Proizvod proizvod)
        {
            if (id != proizvod.Id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _repository.Update(proizvod);
            }
            catch 
            {

                return BadRequest();
            }

            return Ok(proizvod);
        }
        [Route("api/najmanji")]
        public IHttpActionResult GetNajmanji()
        {
            return Ok(_repository.GetAll()
                                 .OrderBy(p => p.CenaProizvoda)
                                 .Take(2));
        }

        public IHttpActionResult GetProizvodi(int kategorija)
        {
            var proizvodi = _repository.GetAll().Where(p => p.Kategorija.Id == kategorija);
            if (proizvodi==null)
            {
                return NotFound();
            }

            return Ok(proizvodi);
        }

        public IHttpActionResult GetProizvodCena(int cena)
        {
            var proizvodi = _repository.GetAll().Where(p => p.CenaProizvoda < cena);

            if (proizvodi==null)
            {
                return NotFound();
            }

            return Ok(proizvodi);
        }

        
    }
}
