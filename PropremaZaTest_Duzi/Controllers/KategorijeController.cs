﻿using PropremaZaTest_Duzi.Interfaces;
using PropremaZaTest_Duzi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PropremaZaTest_Duzi.Controllers
{
    public class KategorijeController : ApiController
    {
        IKategorijaRepository _repository { get; set; }

        public KategorijeController(IKategorijaRepository repository)
        {
            _repository = repository;
        }


        public IHttpActionResult GetKategorije()
        {
            return Ok( _repository.GetAll());
        }

        public IHttpActionResult GetKategorija(int id)
        {
            var kategorija = _repository.GetById(id);

            if (kategorija==null)
            {
                return NotFound();
            }

            return Ok(kategorija);
        }

        public IHttpActionResult DeleteKategorija(int id)
        {
            var kategorija = _repository.GetById(id);

            if (kategorija==null)
            {
                return NotFound();
            }

            _repository.Delete(kategorija);
            return Ok();
        }

        public IHttpActionResult PostKategorija(Kategorija kategorija)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _repository.Create(kategorija);
            return CreatedAtRoute("DefaultApi", new { kategorija.Id }, kategorija);
        }

        public IHttpActionResult PutKategorija(int id, Kategorija kategorija)
        {
            if (id!=kategorija.Id)
            {
                return BadRequest();
            }

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            try
            {
                _repository.Update(kategorija);
            }
            catch 
            {
                return BadRequest();
                
            }

            return Ok(kategorija);
        }
        [Route("api/statistika")]
        public IHttpActionResult GetUkupnaCena()
        {
           return Ok( _repository.GetAll().OrderByDescending(k => k.Proizvodi.Sum(p => p.CenaProizvoda)).Take(2));
        }
            

    }
}
