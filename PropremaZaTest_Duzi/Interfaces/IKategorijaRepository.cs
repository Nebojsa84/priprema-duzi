﻿using PropremaZaTest_Duzi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropremaZaTest_Duzi.Interfaces
{
   public interface IKategorijaRepository
    {
        IEnumerable<Kategorija> GetAll();
        Kategorija GetById(int id);
        void Create(Kategorija kategorija);
        void Delete(Kategorija kategorija);
        void Update(Kategorija kategorija);
    }
}
