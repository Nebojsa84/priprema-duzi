﻿using PropremaZaTest_Duzi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PropremaZaTest_Duzi.Interfaces
{
    public interface IProizvodRepository
    {
        IEnumerable<Proizvod> GetAll();
        Proizvod GetById(int id);
        void Create(Proizvod proizvod);
        void Delete(Proizvod proizvod);
        void Update(Proizvod proizvod);

    }
}
