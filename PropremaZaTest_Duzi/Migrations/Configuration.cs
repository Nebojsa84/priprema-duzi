namespace PropremaZaTest_Duzi.Migrations
{
    using PropremaZaTest_Duzi.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<PropremaZaTest_Duzi.Models.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(PropremaZaTest_Duzi.Models.ApplicationDbContext context)
        {

            context.Kategorije.AddOrUpdate(x => x.Id,
                new Kategorija() { Id=1,Naziv="Namestaj"},
                 new Kategorija() { Id = 2, Naziv = "Bela Tehnika" },
                  new Kategorija() { Id = 3, Naziv = "Elektronika" }
                );

             context.Proizvodi.AddOrUpdate(x => x.Id,
               new Proizvod() { Id = 1, Naziv = "Fotelja", CenaProizvoda = 100, KategorijaId = 1},
               new Proizvod() { Id = 2, Naziv = "Krevet", CenaProizvoda = 150, KategorijaId = 1 },
               new Proizvod() { Id = 3, Naziv = "Sto", CenaProizvoda = 300, KategorijaId = 1 },
               new Proizvod() { Id = 4, Naziv = "Stolica", CenaProizvoda = 70, KategorijaId = 1 },
               new Proizvod() { Id = 5, Naziv = "Ves masina", CenaProizvoda = 200, KategorijaId = 2},
               new Proizvod() { Id = 6, Naziv = "Masina za sudove", CenaProizvoda = 250, KategorijaId = 2 },
               new Proizvod() { Id = 7, Naziv = "Pegla", CenaProizvoda = 50, KategorijaId = 2 },
               new Proizvod() { Id = 8, Naziv = "Ruter", CenaProizvoda = 300, KategorijaId = 3},
               new Proizvod() { Id = 9, Naziv = "Konzola", CenaProizvoda = 100, KategorijaId = 3 }
               );

        }
    }
}
