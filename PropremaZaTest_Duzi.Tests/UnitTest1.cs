﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using PropremaZaTest_Duzi.Controllers;
using PropremaZaTest_Duzi.Interfaces;
using PropremaZaTest_Duzi.Models;

namespace PropremaZaTest_Duzi.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void GetReturnsOKAndObject()
        {
                       
            var MoqRepository = new Mock<IProizvodRepository>();
            MoqRepository.Setup(x => x.GetById(50)).Returns(new Models.Proizvod { Id = 50 });

            var controller = new ProizvodiController(MoqRepository.Object);

            IHttpActionResult actionResult = controller.GetProizvod(50);
            var contentResult = actionResult as OkNegotiatedContentResult<Proizvod>;

            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(50, contentResult.Content.Id);
        }

        [TestMethod]
        public void GetReturnsNotFound()
        {
            var moqRepo = new Mock<IProizvodRepository>();

            var controller = new ProizvodiController(moqRepo.Object);

            IHttpActionResult actionResault = controller.GetProizvod(10);

            Assert.IsInstanceOfType(actionResault, typeof(NotFoundResult));
        }


        [TestMethod]
        public void DeleteReturnsOk()
        {
            var moqRepo = new Mock<IProizvodRepository>();
            moqRepo.Setup(x => x.GetById(10)).Returns(new Proizvod { Id = 10 });

            var controller = new ProizvodiController(moqRepo.Object);

            IHttpActionResult actionResult = controller.DeleteProizvod(10);

            var contentResult = actionResult as OkResult;

            Assert.IsInstanceOfType(contentResult, typeof(OkResult));
        }


        [TestMethod]
        public void DeleteReturnsNotFound()
        {
           var moqRepo = new Mock<IProizvodRepository>();

            var controller = new ProizvodiController(moqRepo.Object);
            var actionResult = controller.DeleteProizvod(10);

            Assert.IsInstanceOfType(actionResult, typeof(NotFoundResult));

        }

        [TestMethod]
        public void POSTReturnsCreatedAtLocation()
        {
            var moqRepo = new Mock<IProizvodRepository>();
            var controller = new ProizvodiController(moqRepo.Object);

            IHttpActionResult actionResult = controller.PostProizvod(new Proizvod { Id = 10, Naziv = "Test" });
            var contentResult = actionResult as CreatedAtRouteNegotiatedContentResult<Proizvod>;


            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual("DefaultApi", contentResult.RouteName);
            Assert.AreEqual(10, contentResult.Content.Id);

        }
       
        [TestMethod]
        public void GetReturnsMultipleObjects()
        {
            List<Proizvod> proizvodi = new List<Proizvod>();
            proizvodi.Add(new Proizvod { Id = 1, Naziv = "Test1", CenaProizvoda = 100 });
            proizvodi.Add(new Proizvod { Id = 2, Naziv = "Test2", CenaProizvoda = 150 });
            proizvodi.Add(new Proizvod { Id = 3, Naziv = "Test3", CenaProizvoda = 200 });

            var moqRepo = new Mock<IProizvodRepository>();
            moqRepo.Setup(x => x.GetAll()).Returns(proizvodi.AsEnumerable);

            var controller = new ProizvodiController(moqRepo.Object);

            IEnumerable<Proizvod> result = controller.GetProizvodi();

            Assert.IsNotNull(result);
            Assert.AreEqual(result.ToList().Count, proizvodi.Count);
            Assert.AreEqual(result.ElementAt(0), proizvodi.ElementAt(0));
            Assert.AreEqual(result.ElementAt(1), proizvodi.ElementAt(1));
            Assert.AreEqual(result.ElementAt(2), proizvodi.ElementAt(2));
        }
    }
    
}
